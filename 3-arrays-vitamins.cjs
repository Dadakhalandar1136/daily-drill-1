const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/



// 1. Get all items that are available 

const allItems = items.filter(element => {
    return element.available;
});

// 2. Get all items containing only Vitamin C.

const containVitaminC = items.filter(element => {
    return element.contains == 'Vitamin C';
});

// 3. Get all items containing Vitamin A.
const containVitaminA = items.filter(element => {
    return element.contains.includes('Vitamin A');
});


// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }

//         and so on for all items and all Vitamins.

const groupItems = items.reduce((accumulator, currentValue) => {
    const conatainValue = currentValue.contains.split(',')
        .map(item => {
            item = item.trim();
            if (accumulator.hasOwnProperty(item)) {
                accumulator[item].push(currentValue.name);
            } else {
                accumulator[item] = currentValue.name;
            }
            return accumulator;
        })
    return conatainValue;
})


// 5. Sort items based on number of Vitamins they contain.

const sortVitamins = items.sort((start, end) => {
    let vitaminA = start.contains.split(',').length;
    let vitaminB = end.contains.split(',').length;
    if (vitaminA < vitaminB) {
        return -1;
    } else {
        return 1;
    }
});