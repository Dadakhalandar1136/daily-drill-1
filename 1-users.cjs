const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*
 
Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.
 
NOTE: Do not change the name of this file
 
*/


// Q1 Find all users who are interested in playing video games.
const usersInterest = Object.entries(users).filter(element => {
    const listUsers = element[1].interests[0].includes('Video Games');
    return listUsers;
});


// Q2 Find all users staying in Germany.
const userCountry = Object.entries(users).filter(element => {
    const listCountry = element[1].nationality == 'Germany';
    return listCountry;
});


// Q3 Sort users based on their seniority level 
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10

const userSenioritySort = Object.entries(users).sort((start, end) => {
    const seniorDeveloper = start[1].desgination;
    const developer = end[1].desgination;
    if (seniorDeveloper < developer) {
        return 1;
    } else {
        return -1;
    }
});


// Q4 Find all users with masters Degree.
const userDegree = Object.entries(users).filter(element => {
    const masterDegree = element[1].qualification.includes('Masters');
    return masterDegree;
});



// Q5 Group users based on their Programming language mentioned in their designation.
const groupUsers = Object.entries(users).reduce((accumulator, currentValue) => {
    accumulator['Javascript'] = [];
    accumulator['Python'] = [];
    accumulator['Golang'] = [];

    if (currentValue[1].desgination.indexOf('Python') != -1) {
        accumulator['Python'].push(currentValue[0]);
    }
    if (currentValue[1].desgination.indexOf('Javascript') != -1) {
        accumulator['Javascript'].push(currentValue[0]);
    }
    if (currentValue[1].desgination.indexOf('Golang') != -1) {
        accumulator['Golang'].push(currentValue[0]);
    }
    return accumulator;
}, {});
