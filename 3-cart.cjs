function cartFunction() {

    const products = [{
        shampoo: {
            price: "$50",
            quantity: 4
        },
        "Hair-oil": {
            price: "$40",
            quantity: 2,
            sealed: true
        },
        comb: {
            price: "$12",
            quantity: 1
        },
        utensils: [
            {
                spoons: { quantity: 2, price: "$8" }
            }, {
                glasses: { quantity: 1, price: "$70", type: "fragile" }
            }, {
                cooker: { quantity: 4, price: "$900" }
            }
        ],
        watch: {
            price: "$800",
            quantity: 1,
            type: "fragile"
        }
    }]


    /*
    
    Q1. Find all the items with price more than $65.
    Q2. Find all the items where quantity ordered is more than 1.
    Q.3 Get all items which are mentioned as fragile.
    Q.4 Find the least and the most expensive item for a single quantity.
    Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
    
    NOTE: Do not change the name of this file
    
    */


    // Q1. Find all the items with price more than $65.
    const listPrice = Object.entries(products[0]).map((item) => {
        if (Array.isArray(item[1])) {
            const findPrice = item[1].filter((element) => {
                let store = Object.entries(element).flat(1);
                return Number(store[1].price.replace('$', '')) > 65;
            })
            return findPrice;
        } else {
            return item;
        }
    })
        .filter((list) => {
            if (typeof list[0] === 'object') {
                return true;
            } else {
                return Number(list[1].price.replace('$', '')) > 65;
            }
        });
        return listPrice;
        // console.log(listPrice);


    // Q2. Find all the items where quantity ordered is more than 1.
    const quantityOrderMoreThanOne = Object.entries(products[0]).map((item) => {
        if (Array.isArray(item[1])) {
            const quantityOrder = item[1].filter((element) => {
                let store = Object.entries(element).flat(1);
                return Number(store[1].quantity) > 1;
            })
            return quantityOrder;
        } else {
            return item;
        }
    })
        .filter((quantityList) => {
            if (typeof quantityList[0] === 'object') {
                return true;
            } else {
                return Number(quantityList[1].quantity) > 1;
            }
        });
        return quantityOrderMoreThanOne;
        // console.log(quantityOrderMoreThanOne);


    // Q.3 Get all items which are mentioned as fragile.
    const allItemsAsFragile = Object.entries(products[0]).map((item) => {
        if (Array.isArray(item[1])) {
            const listFragile = item[1].filter((element) => {
                let store = Object.entries(element).flat(1);
                return store[1].type == 'fragile';
            })
            return listFragile;
        } else {
            return item;
        }
    })
        .filter((fragileItems) => {
            if (typeof fragileItems[0] === 'object') {
                return Object.entries(fragileItems);
            } else {
                return fragileItems[1].type == 'fragile';
            }
        });
        // console.log(allItemsAsFragile);
        return allItemsAsFragile;


    // const leastMostExpensiveItem = Object.entries(products[0]).reduce((max, min) => {
        
    // })
    
}

console.log(cartFunction());